# arkui-x demo

## 简介
本示例通过[ArkUI-X] ace create project创建的一个跨平台工程

![android_main](xdocs/devices/android_main.jpg)

![android_state](xdocs/devices/android_state.jpg)

## 相关概念

* Bridge

## 相关权限

不涉及。

## 使用说明

1.打开app，首页面显示一个button和一个Text。
2.点击Button按键，Text内容发生改变。

## 约束与限制

1.本示例支持在Android\iOS\OpenHarmony平台上运行。

2.本示例需要使用DevEco Studio 4.1 Release及以上版本才可编译运行，ios最好为13版本及以上。
